#include <visp3/core/vpDummy.h>

vpDummy::vpDummy() : m_dummy(0)
{
  std::cout << "In vpDummy::vpDummy()" << std::endl;
}

vpDummy::~vpDummy()
{
  std::cout << "In vpDummy::~vpDummy()" << std::endl;
}

void vpDummy::set(int dummy)
{
  std::cout << "In vpDummy::set()" << std::endl;

  m_dummy = dummy;
}

int vpDummy::get() const
{
  std::cout << "In vpDummy::get()" << std::endl;

  return m_dummy;
}

