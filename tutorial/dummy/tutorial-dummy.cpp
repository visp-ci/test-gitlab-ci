#include <visp3/core/vpDummy.h>

int main()
{
  vpDummy dummy;
  
  int val = 10;
  dummy.set(val);

  if (val == dummy.get())
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}

