#ifndef vpDummy_h
#define vpDummy_h

#include <visp3/core/vpConfig.h>
#include <iostream>

class VISP_EXPORT vpDummy
{
public:
  vpDummy();
  virtual ~vpDummy();
  void set(int dummy);
  int get() const;
  
protected:
  int m_dummy;
};

#endif

